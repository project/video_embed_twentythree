CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Sponsors
 * Maintainers

INTRODUCTION
------------
Embed TwentyThree videos into your website using Video Embed Field.

The module only support TwentyThree HTTPS URL's in the format:

* https://[domain.tld]/v.ihtml/player.html?photo_id=12345678
* https://[domain.tld]/video/12345678/......

domain.tld is configured in Drupal.

REQUIREMENTS
------------
* Video Embed Field
  https://drupal.org/project/video_embed_field

INSTALLATION
------------
* Install the module and required modules.

CONFIGURATION
-------------
* Go to /admin/config/media/twentythree-widget.
* Add the domains used by your TwentyThree account. One domain per line.

TROUBLESHOOTING
---------------
* Submit an issue at
  https://www.drupal.org/project/issues/video_embed_twentythree.

SPONSORS
--------
* FFW - https://ffwagency.com

MAINTAINERS
-----------
Current maintainers:
* Jens Beltofte (beltofte) - https://drupal.org/u/beltofte
