<?php

namespace Drupal\video_embed_twentythree\Plugin\video_embed_field\Provider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\video_embed_field\ProviderPluginBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A TwentyThree provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "twentythree",
 *   title = @Translation("TwentyThree")
 * )
 */
class TwentyThree extends ProviderPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The http client service.
   *
   * @var GuzzleHttp\Client
   *
   * @see GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs the TwentyThree object.
   *
   * @param array $configuration
   *   The configuration of the plugin.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory services.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $http_client, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client);
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
  }

  /**
   * Creates the TwentyThree object.
   *
   * @param Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The factory for configuration objects.
   * @param array $configuration
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $plugin_id
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $plugin_definition
   *   The factory for configuration objects.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $config = $this->configFactory->get('video_embed_twentythree.settings');
    $domain = parse_url($this->getInput(), PHP_URL_HOST);

    // Auto mute when video is auto-playing to avoid video being blocked by
    // some browsers including Chrome and Firefox.
    $autoplay = intval($autoplay);
    if ($autoplay && $config->get('automute_autoplay_videos')) {
      $automute = 1;
    }
    else {
      $automute = 0;
    }

    $embed_code = [
      '#type' => 'video_embed_iframe',
      '#provider' => 'twentythree',
      '#url' => 'https://' . $domain . '/v.ihtml/player.html',
      '#query' => [
        'photo_id' => $this->getVideoId(),
        'autoPlay' => $autoplay,
        'autoMute' => $automute,
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];

    // Add query parameters from embed URL if enabled.
    if ($config->get('enable_query_parameters') && !empty($config->get('allowed_query_parameters'))) {
      $query_parameters = $this->getEmbedQueryParameters();

      // Adding token if it was either in the query string or in the url path.
      if (!empty($config->get('allowed_query_parameters')['token']) && $token = $this->getToken($this->getInput())) {
        $embed_code['#query']['token'] = $token;
      }

      if (!empty($query_parameters)) {
        $embed_code['#query'] = array_merge($embed_code['#query'], $query_parameters);
      }
    }

    return $embed_code;
  }

  /**
   * Get the TwentyThree oembed data.
   *
   * @return array|false
   *   An array of data from the oembed endpoint.
   */
  protected function oEmbedData() {
    $url = $this->getInput();
    $url_parts = parse_url($url);
    $query = [
      'url' => $url,
      'format' => 'json',
    ];
    $oembed_url = $url_parts['scheme'] . '://' . $url_parts['host'] . '/oembed?';

    try {
      $request = $this->httpClient->get($oembed_url, ['query' => $query]);
      if ($request->getStatusCode() == 200) {
        $json = json_decode($request->getBody()->getContents());
        return $json ? $json : FALSE;
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }

    return FALSE;
  }

  /**
   * Get the TwentyThree video data.
   *
   * @return array|false
   *   An array of data from the api endpoint.
   */
  protected function apiVideoData() {
    $url = $this->getInput();
    $url_parts = parse_url($url);
    $query = [
      'photo_id' => $this->getIdFromInput($url),
      'token' => $this->getToken($url),
      'format' => 'json',
    ];
    $api_url = $url_parts['scheme'] . '://' . $url_parts['host'] . '/api/photo/list';

    try {
      $request = $this->httpClient->get($api_url, ['query' => $query]);
      if ($request->getStatusCode() == 200) {
        $body = $request->getBody()->getContents();
        // Strip "var visual = " from beginning of JSON data to make it valid.
        if (strpos($body, 'var visual = ') === 0) {
          $body = substr($body, strpos($body, '{'));
        }
        $json = json_decode($body);

        if (!empty($json->photos[0])) {
          $video_data = $json->photos[0];
          $video_data->__base_url = $url_parts['scheme'] . '://' . $url_parts['host'];
          return $video_data;
        }
        else {
          return FALSE;
        }
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    if ($this->getToken()) {
      $this->apiVideoData()->title;
    }
    else {
      return $this->oEmbedData()->title;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    if ($this->getToken()) {
      return $this->apiVideoData()->__base_url . '/' . $this->apiVideoData()->large_download;
    }
    else {
      return $this->oEmbedData()->thumbnail_url;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    $config = \Drupal::config('video_embed_twentythree.settings');
    $domains = str_replace("\n", '|', $config->get('video_domains'));
    $url = urldecode($input);

    $matches1 = $matches2 = $matches3 = [];
    preg_match("/^https?:\/\/({$domains})\/video\/(?<id>[a-z0-9]+).*$/", $url, $matches1);
    preg_match("/^https?:\/\/({$domains})\/secret\/(?<id>[a-z0-9]+).*\/(?<token>[a-z0-9]+).*$/", $url, $matches2);
    preg_match("/^https?:\/\/({$domains})\/.*?.ihtml\/player.html\?.*photo_id=(?<id>[0-9]+).*$/", $url, $matches3);

    if ($matches1 && !empty($matches1['id'])) {
      return (int) $matches1['id'];
    }
    elseif ($matches2 && !empty($matches2['id']) && !empty($matches2['token'])) {
      return (int) $matches2['id'];
    }
    elseif ($matches3 && !empty($matches3['id'])) {
      return (int) $matches3['id'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get the start time from the URL.
   *
   * @return string|false
   *   A start parameter to pass to the frame or FALSE if none is found.
   */
  protected function getStartTime() {
    preg_match('/\?.*start=(?<start>(\d+)s)$/', $this->input, $matches);
    return $matches['start'] ?? FALSE;
  }

  /**
   * Get query parameters from the embed URL.
   *
   * Filter them based on allowed parameters.
   *
   * @return array
   *   List with filtered query parameters.
   */
  protected function getEmbedQueryParameters() {
    $url = $this->getInput();
    $config = $this->configFactory->get('video_embed_twentythree.settings');
    $allowed_parts = $config->get('allowed_query_parameters');
    $result = [];

    // Parse query string.
    $query_string = parse_url($url, PHP_URL_QUERY);
    if (!empty($query_string)) {
      parse_str($query_string, $query_parts);
    }

    // Filter unwanted query parameters.
    if (!empty($query_parts) && is_array($query_parts)) {
      foreach ($allowed_parts as $key) {
        if (isset($query_parts[$key])) {
          $result[$key] = $query_parts[$key];
        }
      }
    }

    return $result;
  }

  /**
   * Get token from the URL if exists.
   *
   * Private TwentyThree video sites use URL tokens when videos are shared
   * public.
   *
   * @return string|false
   *   The token parameter to include in the video embed or
   *   FALSE if none is found.
   */
  protected function getToken($input = '') {
    $input = empty($input) ? $this->getInput() : $input;
    // Parse query string.
    $query_string = parse_url($input, PHP_URL_QUERY);
    if (!empty($query_string)) {
      parse_str($query_string, $query_string);
    }

    // Parse url path.
    $url_path = parse_url($input, PHP_URL_PATH);

    // Get token parameter from query string.
    if (!empty($query_string['token'])) {
      return $query_string['token'];
    }
    // Check if the path contains a token.
    else {
      $matches = [];
      preg_match("/^\/secret\/(?<id>[a-z0-9]+).*\/(?<token>[a-z0-9]+).*$/", $url_path, $matches);

      if (!empty($matches['token'])) {
        return $matches['token'];
      }
    }
    return FALSE;
  }

}
